///BY RWAID KALKA
void main() {
  final largestNumbers = kLargestElementCount(3, [10, 20, 20, 30, 40]);
  print('Largest 3 Numbers in [10, 20, 20, 30, 40] are: $largestNumbers');
  final sortedList = sortList([10, 0, -2, 6, 1]);
  print('Sorted List of [10, 0, -2, 6, 1] is: $sortedList');

  final distinctNumbers = distinctValue([10, 11, 11, 2, 2, 4, 7, 1, 10]);
  print(
      'Distinct Numbers of [10, 11, 11, 2, 2, 4, 7, 1, 10] are: $distinctNumbers');

  final days = daysNumberForTheFrogToClimb(30);
  print(
      'Days Needed From The Frog To Get Outside Of The 30 meters wall Are: $days');

  final results = stringsContainedInLargeString(
    'Hello World My Name is Rwaid',
    ['Hello', 'Rwaid', 'is', 'Ok', 'Buy'],
  );

  print(
      '[Hello, Rwaid, is, Ok, Buy] results contained in this "Hello World My Name is Rwaid" are : $results');
}

List<int> kLargestElementCount(int count, List<int> list) {
  assert(list.isNotEmpty && list.length > count);

  final largestNumbers = <int>[];
  int largestNumber = list.first;
  for (int i = 0; i < list.length; i++) {
    if (list[i] > largestNumber) {
      largestNumber = list[i];
    }
    if (i == list.length - 1 && largestNumbers.length != count) {
      list.remove(largestNumber);
      largestNumbers.add(largestNumber);
      largestNumber = list.first;

      /// Reset Counter This -1 is because it will increase by one and become 0 for next iteration

      i = -1;
    }
  }

  return largestNumbers;
}

List<int> sortList(List<int> list) {
  assert(list.isNotEmpty);

  int holder;
  for (int i = 0; i < list.length; i++) {
    ///Check if the index is less than length and check if first element id greater than second element
    if (i + 1 < list.length && list[i] > list[i + 1]) {
      holder = list[i];
      list[i] = list[i + 1];
      list[i + 1] = holder;

      /// Reset Counter This -1 is because it will increase by one and become 0 for next iteration
      i = -1;
    }
  }

  return list;
}

List<int> distinctValue(List<int> list) {
  assert(list.isNotEmpty);

  final distinctValue = <int>[];

  for (int i = 0; i < list.length; i++) {
    if (!distinctValue.contains(list[i])) {
      distinctValue.add(list[i]);
    }
  }

  /// or we can use the toSet() function on the list it will only return the distinct values
  /// list.toSet().toList();

  return distinctValue;
}

List<bool> stringsContainedInLargeString(
    String largeString, List<String> smallStrings) {
  assert(smallStrings.isNotEmpty);

  final result = <bool>[];

  for (int i = 0; i < smallStrings.length; i++) {
    result.add(largeString.contains(smallStrings[i]));
  }

  return result;
}

int daysNumberForTheFrogToClimb(int wallDepth) {
  assert(wallDepth > 0);
  const upEachDay = 3;
  const downEachNight = 2;

  ///This is because that at last day it won't get down it will get outside before night
  final days = (wallDepth - downEachNight) ~/ (upEachDay - downEachNight);

  return days;
}
